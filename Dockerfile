FROM node:alpine
WORKDIR /usr/src/app
RUN npm i ring-client-api
RUN npm i system-commands
RUN npm i typescript
RUN npm i ts-node
ADD . /usr/src/app
RUN npm audit fix
#CMD [ "ls", "/usr/src/app" ]
CMD [ "./node_modules/ts-node/dist/bin.js", "ring.ts" ]
