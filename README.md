## How I run it?  

1. Clone or download this repo where ever you want on the machine that will run the docker instance
1. On some machine with npm installed (Not within this container) run `npx -p ring-client-api ring-auth-cli`
1. Stick that token into `./ring.token` (Yes that's right, in this directory)
1. `sudo docker-compose up --build` (add `-d` to detach the container and have it run in the background)

## What do I need to do in my Ring App?  
If you don't have a ring app, use the one online on ring's website.  

All of the cameras that you want to record video from when there is motion **must** have motion enabled in all desired modes.  You can enable motion detection (and therefore recording with this script) by navigating the following:  
1. Open App
1. Open the Settings page (on Android this is from the left side panel)
1. Modes
1. {Disarmed, Home, Away}
1. Cameras
1. Select a camera
1. Make sure Motion Detection is enabled  

By default this app will still only ever record non-doorbell cameras in away mode regardless of these app settings.  However if motion detection is disabled on a camera here for away mode, it will never record regardless of what this script is programmed to do.


## What's default?  
* It will record to `./output` (Change that in the `docker-compose.yml` file (change source not target))
* It will always record video motions from a camera that happens to be named `Food Cam` if it has motion detection enabled in the Ring App.  This should help you learn how to customize the script to record from other cameras at different times :)
* It will always record video motions from ring doorbells (if they are version 3?)
* It will record all other video motions only if the ring system is in away mode

## What if I have multiple ring locations?  
I'm not sure... I'm too lazy to look at the code and answer that question right now :)

## Customization  
Hopefully one day you can customize what gets recorded when and from which camera via environment variables but I am not there yet.  
The `ring.ts` file is not very large and you might be able to change those sorts of things with a little knowledge of Javascript.

#### Tiny Note
I don't actually like typescript... ;)
