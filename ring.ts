import "dotenv/config";
import { RingApi, RingCameraKind } from "ring-client-api";
import { skip } from "rxjs/operators";
import { readFile, writeFile } from "fs";
import { promisify } from "util";

async function example() {
  let outPath = "/output";
  let tokenPath = "/usr/src/app/ring.token";

  var fs = require("fs");
  var system = require("system-commands");
  var myToken = fs.readFileSync(tokenPath, { encoding: "utf8" });

  const ringApi = new RingApi({
    refreshToken: myToken,
    cameraDingsPollingSeconds: 2,
  }),
    locations = await ringApi.getLocations(),
    allCameras = await ringApi.getCameras();

  console.log(`Found ${locations.length} location(s) with ${allCameras.length} camera(s).`);

  ringApi.onRefreshTokenUpdated.subscribe(async ({ newRefreshToken, oldRefreshToken }) => {
    console.log("Refresh Token Updated: ", newRefreshToken);

    // If you are implementing a project that use `ring-client-api`, you should subscribe to onRefreshTokenUpdated and update your config each time it fires an event
    // Here is an example using a .env file for configuration
    if (!oldRefreshToken) {
      return;
    }

    const currentConfig = await promisify(readFile)(tokenPath),
      updatedConfig = currentConfig.toString().replace(oldRefreshToken, newRefreshToken);

    await promisify(writeFile)(tokenPath, updatedConfig);
  });

  for (const location of locations) {
    location.onConnected.pipe(skip(1)).subscribe(async (connected) => {
      const status = connected ? "Connected to" : "Disconnected from";
      console.log(`**** ${status} location ${location.name} - ${location.id}`);
    });
  }

  for (const location of locations) {
    const cameras = location.cameras,
      devices = await location.getDevices();

    console.log(`\nLocation ${location.name} has the following ${cameras.length} camera(s):`);

    for (const camera of cameras) {
      console.log(`- ${camera.id}: ${camera.name} (${camera.deviceType})`);
    }

    console.log(`\nLocation ${location.name} has the following ${devices.length} device(s):`);

    for (const device of devices) {
      console.log(`- ${device.zid}: ${device.name} (${device.deviceType})`);
    }
  }

  var current_recordings: string[] = [];

  if (allCameras.length) {
    allCameras.forEach((camera) => {
      console.log("Adding camera!");
      camera.onNewDing.subscribe(async (ding) => {
        const event = ding.kind === "motion" ? "Motion detected" : ding.kind === "ding" ? "Doorbell pressed" : `Video started (${ding.kind})`;

        console.log(`${event} on ${camera.name} camera. Ding id ${ding.id_str}.  Received at ${new Date()}`);
        if (ding.kind === "ding") {
          // system("echo ding | nc -N 192.168.1.18 6789");
        }

        if (ding.kind === "motion") {
          var cri = current_recordings.indexOf(ding.id_str);
          if (cri == -1 && (camera.name == "Food Cam" || camera.deviceType === RingCameraKind.doorbell_v3 || (await locations[0].getAlarmMode()) === "all")) {
            cri = current_recordings.push(ding.id_str) - 1;
            var record_time = 60;
            if (camera.deviceType === RingCameraKind.doorbell_v3) {
              record_time = 30;
            }
            let date: Date = new Date();
            console.log("Recording video now!");
            let date_string = `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}_${("0" + date.getHours()).slice(-2)}:${("0" + date.getMinutes()).slice(-2)}:${("0" + date.getSeconds()).slice(-2)}`;
            let file_name = `${date_string}_${camera.name}.mp4`;
            let final_path = `${outPath}/${file_name}`;
            let temp_path = `/tmp/${file_name}`;
            await camera.recordToFile(temp_path, record_time);
            current_recordings.splice(cri, 1);
            system(`mv '${temp_path}' '${final_path}'`);
            console.log("Done recording video.");
          }
        }
      });
    });

    console.log("Listening for motion and doorbell presses on your cameras.");
  }
}

example();
